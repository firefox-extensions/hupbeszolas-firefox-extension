# Safari extension

To use this extension in safari, just create the extension in `extension builder` with the directory `HUPbeszolas.safariextension`

For detailed instructions how to create the Safari extension, [see the wiki here](https://gitlab.com/firefox-extensions/hupbeszolas-firefox-extension/wikis/safari-extension-howto)
