﻿// --- GLOBALS ---
var rightMenuText = "MENÜ";
var ownCommentColor = '#FFF0A0';
var trollCommentColor = '#FF7070';
var defaultTrollCommentColor = '#FF7070';
var defaultOwnCommentColor = '#FFF0A0';
var normalCommentColor = '#D8D8C4';
var cookies = document.cookie.split(";");
var hup = new Array();
var hiddenCommentsCount = 0;
var userName = "";
var userID = "";
var originalTitle = "";
var showTrolls = "1";

// FUNCTIONS

function getElementsByClassName2(node, classname) {
	var a = [];
	var re = new RegExp(classname);
	var els = node.getElementsByTagName("*");
	for(var i=0,j=els.length; i<j; i++)
		if(re.test(els[i].className))a.push(els[i]);
	return a;
}

function getAllComments(node) {
	return getElementsByClassName2(node, '^comment( comment-new)?$');
}

function getCommentContent(commentNode) {
	return commentNode.getElementsByClassName('content')[0].getElementsByTagName('P')[0].innerHTML;	
}

function getCommentContentElement(commentNode) {
	return commentNode.getElementsByClassName('content')[0];	
}

function getCommentContentHasMoreBlocks(commentNode) {
	return commentNode.getElementsByClassName('content')[0].getElementsByTagName('P').length > 1;
}

function getCommentAllContent(commentNode) {
	var allContent = "";
	var commentBlocks = commentNode.getElementsByClassName('content')[0].getElementsByTagName('P');
	for (z=0;z<commentBlocks.length;z++) {
		allContent += commentBlocks[z].innerHTML;
	}
	return allContent;
}
	

function getCommentSubmitted(commentNode) {
	return commentNode.getElementsByClassName('submitted')[0];
}

function getCommentUserName(commentNode) {
	return commentNode.getElementsByClassName('submitted')[0].getElementsByTagName('A')[0].innerHTML;
}

function getCommentUserLink(commentNode) {
	return commentNode.getElementsByClassName('submitted')[0].getElementsByTagName('A')[0].href;
}

function getCommentLinkBlock(commentNode) {
	return commentNode.getElementsByClassName('link')[0];
}

function getCommentLinkBlockUL(commentNode) {
	return commentNode.getElementsByClassName('link')[0].getElementsByTagName('UL')[0];
}

function hideComment(commentNode) {
	commentNode.style.display = 'none';
}

function unhideComment(commentNode) {
	commentNode.style.display = 'block';
}

function isTrollComment(commentNode) {
	if (typeof hup['trolls'] == 'undefined') return false;
	var trolls = hup['trolls'].split(",");
	var uname = getCommentUserName(commentNode);
	for (z=0;z<trolls.length;z++) {
		if (uname == trolls[z]) return true;
	}
	return false;
}

function getUserColorAssociated(commentNode) {
	var uname = getCommentUserName(commentNode);
	if (typeof hup['color_'+uname] != 'undefined') {
		if ((hup['color_'+uname] != " ")&&(hup['color_'+uname] != ""))
			return hup['color_'+uname];
		else
			return normalCommentColor;
	} else return normalCommentColor;
}

function getCommentID(commentNode) {
	var Cprev = commentNode.previousSibling;
	while (Cprev.tagName != "A") Cprev = Cprev.previousSibling;
	return Cprev.id;
}





function loadCookies() {
	hup = new Array();
	for (i=0;i<cookies.length;i++) {
		var c = cookies[i];			
		c = c.replace(/^\s+|\s+$/,"");
		if (c.indexOf('hup_') == 0)
			hup[c.substring(4, c.indexOf("="))] = c.substring(c.indexOf('=')+1);
	}
}

function saveCookies() {
	var expires = new Date();
	expires.setDate(expires.getDate() + 365);
	for (var key in hup) {
		document.cookie = 'hup_'+key+'='+hup[key]+'; expires='+expires.toUTCString()+'; path=/';
	}
}

function deleteCookie(key) {
	delete hup[key];
	document.cookie = 'hup_'+key+'= ';
	saveCookies();
}

function colorizeComments() {
	var comments = getAllComments(document);
	for (i=0;i<comments.length;i++) {
		var comment = comments[i];
		var submitted = getCommentSubmitted(comment);
		if (isTrollComment(comment)) {				
			submitted.style.backgroundColor = trollCommentColor;
		} else {
			submitted.style.backgroundColor = getUserColorAssociated(comment);		
		}
	}
}

function setTroll(username) {
	if (typeof hup['trolls'] == 'undefined') {
		hup['trolls'] = username;
	} else {
		if (hup['trolls'] != "") hup['trolls'] += ',';
		hup['trolls'] += username;
	}
	updateAll();
}

function unsetTroll(username) {
	if (typeof hup['trolls'] != 'undefined') {
		var trolls = hup['trolls'].split(",");
		var newTrolls = "";
		for (z=0;z<trolls.length;z++) {
			var troll = trolls[z];
			if ((troll != username)&&(troll != "")) {				
				newTrolls += troll;
				if (z<trolls.length-1) newTrolls += ',';
			}
		}
		hup['trolls'] = newTrolls;			
	}
	updateAll();
}

function isTroll(username) {
	if (typeof hup['trolls'] != 'undefined') {
		var trolls = hup['trolls'].split(",");
		for (z=0;z<trolls.length;z++) {
			if (trolls[z] == username) return true;
		}
	}
	return false;
}

function toggleTroll(username) {
	if (isTroll(username))
		unsetTroll(username);
	else
		setTroll(username);
}



function switchTrollShow() {
	if (showTrolls != "1") {
		showTrolls = "1";
	} else {
		showTrolls = "0";
	}
	hup['showTrolls'] = showTrolls;
	updateAll();
}

function showAllComments() {
	var comments = getAllComments(document);
	for (i=0;i<comments.length;i++) {
		unhideComment(comments[i]);
	}
}

function hideComments() {
	var comments = getAllComments(document);
	for (i=0;i<comments.length;i++) {
	var comment = comments[i];
	var content = getCommentContent(comment);
	if (
		(content == '-') ||
		(content == '--') ||
		(content == '.') ||
		
		(comment.plusoneHide == "1") ||
		((showTrolls!="1")&&(isTrollComment(comment)))		
		) {
			hideComment(comment);
			hiddenCommentsCount++;
		}
	}
}

function updateMenuTrollList() {
	var list = document.getElementById('menuTrollList');
	if (list == null) {
		alert('nullbazzeg');
		return;
	}
	
	list.innerHTML = "";
	if (typeof hup['trolls'] != 'undefined') {
		var trolls = hup['trolls'].split(",");
		var tcount = 0;
		for (z=0;z<trolls.length;z++) {
			if (trolls[z] != "") {
				list.innerHTML += '&bull;&nbsp;<a href="javascript:unsetTroll(\''+trolls[z]+'\')">'+trolls[z]+'</a><br/>';
				tcount++;
			}
		}
		if (tcount == 0) {
			list.innerHTML = 'üres :)';
		}
	}	
}

function updateAll() {
	saveCookies();
	colorizeComments();
	showAllComments();
	hideComments();
	updateMenuTrollList();
}


// --- MAIN ---

loadCookies();
saveCookies();


// --- Get variables from cookie ---
if (typeof hup['ownColor'] != 'undefined') ownCommentColor = hup['ownColor'];
if (typeof hup['trollColor'] != 'undefined') trollCommentColor = hup['trollColor'];
if (typeof hup['showTrolls'] != 'undefined') showTrolls = hup['showTrolls'];

// getting user name and id
var userBlock = document.getElementById('block-user-1');
var userBlockLinks = userBlock.getElementsByTagName('A');
originalTitle = document.title;
userName = userBlock.getElementsByTagName('H2')[0].innerHTML;
userID = "";
for (i=0;i<userBlockLinks.length;i++) {
	if (userBlockLinks[i].innerHTML == 'saját adatok') {
		userID = userBlockLinks[i].href;
		userIDlio = userID.lastIndexOf('/');
		userID = userID.substring(userIDlio+1);
	}
}

// setting own color
hup['color_'+userName] = ownCommentColor;

// powered :)
var topnav = document.getElementById('top-nav');
var mydiv = document.createElement("DIV");
mydiv.innerHTML = 'HUPbeszolas (0.5.0)';
mydiv.style.cssFloat = 'left';
mydiv.style.color = 'darkgray';
mydiv.style.width = '200px';
mydiv.style.textAlign = 'left';
topnav.appendChild(mydiv);


//updateAll();


// appending comments_per_page parameter to the links
links = document.getElementsByTagName("A");
for (i=0; i<links.length; i++) {			
	elem = links[i];
	elemHref = elem.href.replace(/^\s+/,"");			
	if (
		(elemHref.indexOf('http://hup.hu/node/') == 0) ||
		(elemHref.indexOf('http://hup.hu/cikkek/') == 0) ||
		(elemHref.indexOf('http://hup.hu/promo/') == 0) ) {				
			linkParts = elemHref.split("#");
			elem.href = linkParts[0] + '&comments_per_page=9999';
			if (linkParts.length > 1) elem.href = elem.href + "#" + linkParts[1];
	}
}

// ---

var collection = document.getElementsByClassName('comment_reply');
for (var i=0; i<collection.length; i++) {
	var elem = collection[i];
	if (elem.tagName == "A")
	elem.innerHTML = "beszólok";
}

// ---

var collection = document.getElementsByClassName('comment_add');
for (var i=0; i<collection.length; i++) {
	var elem = collection[i];
	if (elem.tagName == "A") elem.innerHTML = "beszólás";
}

// ---

var collection = document.getElementsByClassName('comment_comments');
for (var i=0; i<collection.length; i++) {
	var elem = collection[i];
	if (elem.tagName == "A") {
		var cnt = parseInt(elem.innerHTML);
		elem.innerHTML = cnt + " beszólás";			
	}
}

// ---

var collection = document.getElementsByClassName('comment_new_comments');
for (var i=0; i<collection.length; i++) {
	var elem = collection[i];
	if (elem.tagName == "A") {
		var cnt = parseInt(elem.innerHTML);
		elem.innerHTML = cnt + " új beszólás";			
	}
}

// ---

var collection = document.getElementsByTagName('H2');
for (var i=0; i<collection.length; i++) {
	var elem = collection[i];
	if (elem.innerHTML == "Friss hozzászólások") elem.innerHTML = "Friss beszólások";
}

// --- NEW COMMENTS
var newComments = document.getElementsByClassName("new");
for (i=0;i<newComments.length;i++) {
	var newCommentInner = "";			
	/* prev */ if (i>0) newCommentInner += '<a href="#new-'+(i-1)+'">&lt;&lt;</a>&nbsp;';
	/* link */ newCommentInner += 'új&nbsp;<a name="new-'+i+'"></a>';
	/* next */ if (i<newComments.length-1) newCommentInner += '<a href="#new-'+(i+1)+'">&gt;&gt;</a>';
	newComments[i].innerHTML = newCommentInner;			
}

// --- HILIGHT OWN COMMENTS
/*
var ownComments = document.getElementsByClassName('submitted');
for (i=0;i<ownComments.length;i++) {
	var ownCommentsLinks = ownComments[i].getElementsByTagName('A');
	for (j=0;j<ownCommentsLinks.length;j++) {
		if (ownCommentsLinks[j].innerHTML == userName) {
			ownComments[i].style.backgroundColor = ownCommentColor;
		}
	}
}
*/

function showParentCommentBlock(parentDivId) {
	var parentDiv = document.getElementById(parentDivId);
	var copy = parentDiv.cloneNode(true);

	copy.id = 'hupbeszolas_floating_comment';
	copy.style.position = 'fixed';
	copy.style.left = '10px';
	copy.style.top = '10px';
	copy.style.width = ((document.body.clientWidth / 2) - 10) + 'px';
	copy.style.backgroundColor = '#e6e6db';
	copy.style.boxShadow = '6px 6px 20px #888';

	var copyDivs = copy.getElementsByTagName('DIV');
	var firstDivSpans = copyDivs[0].getElementsByTagName('SPAN');
	var firstDivLinks = copyDivs[0].getElementsByTagName('A');
	copyDivs[0].removeChild(firstDivSpans[0]);
	copyDivs[0].removeChild(firstDivSpans[0]);
	if (firstDivLinks.length > 1) {
		copyDivs[0].removeChild(firstDivLinks[1]);
	}
	copyDivs[2].removeChild(copyDivs[2].getElementsByTagName("UL")[0]);

	document.body.appendChild(copy);
}

function removeFloatingComment() {
	var floatingComment = document.getElementById('hupbeszolas_floating_comment');
	if (floatingComment) {
		document.body.removeChild(floatingComment);
	}
}

// --- HANDLE +1
var indentDIVs = document.getElementsByClassName('indented');
var commentDivCurrentIdNum = 0;
for (i=0;i<indentDIVs.length;i++) {

	var plusoneCount = 0;
	var plusoneUsers = new Array();
	var plusoneUsersLink = new Array();

	var indentComments = indentDIVs[i].childNodes;

	// get parent comment's "link" div
	var indentDIVLink = indentDIVs[i].previousSibling;
	while ((indentDIVLink.tagName != "DIV")&&((indentDIVLink.className != "comment")||(indentDIVLink.className != "comment comment-new"))) indentDIVLink = indentDIVLink.previousSibling;

	commentDivCurrentIdNum++;
	indentDIVLink.id = 'comment_div_id_' + commentDivCurrentIdNum;

	indentDIVLink = indentDIVLink.getElementsByClassName('link')[0];

	// get parent comment's id
	var indentDIVLinkP = indentDIVs[i].previousSibling;
	while (indentDIVLinkP.tagName != "A") indentDIVLinkP = indentDIVLinkP.previousSibling;
	indentDIVLinkP = indentDIVLinkP.id;			

	for (j=0;j<indentComments.length;j++) {
		if (indentComments.item(j) instanceof HTMLDivElement) {
			var comment = indentComments.item(j);
			if ((comment.className == 'comment')||(comment.className == 'comment comment-new')) {
				var indentCommentContent   = getCommentContent(comment);
				
				if (indentCommentContent.indexOf('+1') == 0) {
					plusoneCount++;
					plusoneUsers.push(getCommentUserName(comment));
					plusoneUsersLink.push(getCommentUserLink(comment));
				}
				
				if ((indentCommentContent == '+1')&&(!getCommentContentHasMoreBlocks(comment))) {
					comment.plusoneHide = "1";
					hideComment(comment);
					hiddenCommentsCount++;
				}
				
				// insert parent comment link
				getCommentSubmitted(comment).innerHTML += '<a style="float: right" href="#'+indentDIVLinkP+'" onmouseout="removeFloatingComment()" onmouseover="showParentCommentBlock(\'comment_div_id_' + commentDivCurrentIdNum + '\')">&nbsp;&uarr;&uarr;&nbsp;</a>';
			}
		}				
	}

	if (plusoneCount > 0) {
		var plusoneDIV = document.createElement('DIV');
		plusoneDIV.style.textAlign = 'left';
		plusoneDIV.style.color = 'green';
		plusoneDIV.style.marginLeft = '4px';
		plusoneDIV.style.cssFloat = 'left';				
		plusoneDIV.innerHTML = '+'+plusoneCount;
		if (plusoneUsers.length > 0) {
			plusoneDIV.innerHTML += ' (';
			for (x=0;x<plusoneUsers.length;x++) {
				plusoneDIV.innerHTML += '<a href="'+plusoneUsersLink[x]+'">'+plusoneUsers[x]+'</a>';
				if (x<plusoneUsers.length-1) plusoneDIV.innerHTML += ', ';
			}
			plusoneDIV.innerHTML += ')';
		}
		indentDIVLink.insertBefore(plusoneDIV, indentDIVLink.firstChild);
	}
}

// --- LIKE BUTTON ---
var likeComments = getAllComments(document);
for (i=0;i<likeComments.length;i++) {
	var likeComment = likeComments[i];
	var likeLinks = getCommentLinkBlockUL(likeComment);

	var likeReply = "";
	var likeReplyAs = likeComments[i].getElementsByTagName('A');
	for (j=0;j<likeReplyAs.length;j++) {
		if (likeReplyAs[j].className == 'comment_reply') {
			likeReply = likeReplyAs[j]; // reply url
		}
	}

	// add like link (li a class=comment_reply)
	var likeLi = document.createElement('LI');
	likeLi.className = 'comment_reply';
	var likeA = document.createElement('A');
	likeA.className = 'comment_reply';
	likeA.innerHTML = 'lájk';
	likeA.href = likeReply+'&plusone';
				
	likeLi.appendChild(likeA);
	likeLinks.insertBefore(likeLi, likeLinks.firstChild);		
}

// --- COMMENT USER MENU ---

comments = getAllComments(document);
var umCnt = 0;
for (i=0;i<comments.length;i++) {
	var comment = comments[i];
	var subm = getCommentSubmitted(comment);
	var userMenuT = document.createElement("SPAN");
	var userMenuC = document.createElement("SPAN");
		
	userMenuT.innerHTML = "&nbsp;<b>T</b>&nbsp;";
	userMenuT.style.cursor = "pointer";
	userMenuT.commentUserName = getCommentUserName(comment);
	userMenuT.onclick = function() {
		toggleTroll(this.commentUserName);
	};
	
	userMenuC.innerHTML = "&nbsp;<b>C</b>&nbsp;";
	userMenuC.style.cursor = "pointer";
	userMenuC.commentUserName = userMenuT.commentUserName;
	userMenuC.commentObj = comment;
	userMenuC.isSetupActive = 0;
	userMenuC.onclick = function() {
		if (this.isSetupActive != 0) return;
		
		this.isSetupActive = 1;
		
		var cont = getCommentContentElement(this.commentObj);
		this.savedContent = cont.innerHTML;
		
		cont.innerHTML = '';
		
		var userSetP = document.createElement('P');
		var userSetLabel1 = document.createElement('SPAN');
		var userSetInputColor = document.createElement('INPUT');
		var userSetInputSet = document.createElement('INPUT');
		var userSetInputReset = document.createElement('INPUT');
		var userSetInputCancel = document.createElement('INPUT');
		
		userSetLabel1.innerHTML = 'Color: ';
				
		userSetInputColor.type = "TEXT";
				
		var myPicker = new jscolor.color(userSetInputColor, {hash: true});
						
		userSetInputSet.type = "BUTTON";
		userSetInputSet.value = "Set";
		userSetInputSet.userMenuC = this;
		userSetInputSet.savedContent = this.savedContent;
		userSetInputSet.commentObj = this.commentObj;
		userSetInputSet.inputColor = userSetInputColor;
		userSetInputSet.commentUserName = this.commentUserName;
		userSetInputSet.onclick = function() {
			if (typeof this.isTroll != 'undefined') {
				hup['trollColor']=this.inputColor.value;
				trollCommentColor = hup['trollColor'];
			} else if (typeof this.isOwn != 'undefined') {
				hup['ownColor']=this.inputColor.value;
				ownCommentColor = hup['ownColor'];
				hup['color_'+this.commentUserName]=this.inputColor.value;
				
			} else {
				hup['color_'+this.commentUserName]=this.inputColor.value;
			}
			
			this.userMenuC.isSetupActive = 0;
			getCommentContentElement(this.commentObj).innerHTML = this.savedContent;			
			updateAll();
		};
		
		userSetInputReset.type = "BUTTON";
		userSetInputReset.value = "Reset";
		userSetInputReset.userMenuC = this;
		userSetInputReset.savedContent = this.savedContent;
		userSetInputReset.commentObj = this.commentObj;
		userSetInputReset.commentUserName = this.commentUserName;
		userSetInputReset.onclick = function() {
			if (typeof this.isTroll != 'undefined') {
				hup['trollColor']=defaultTrollCommentColor;
				trollCommentColor = hup['trollColor'];
			} else if (typeof this.isOwn != 'undefined') {
				hup['ownColor']=defaultOwnCommentColor;
				hup['color_'+this.commentUserName]=defaultOwnCommentColor;
				ownCommentColor = hup['ownColor'];				
			} else {
				hup['color_'+this.commentUserName]=' ';
				deleteCookie('color_'+this.commentUserName);
			}
			this.userMenuC.isSetupActive = 0;
			getCommentContentElement(this.commentObj).innerHTML = this.savedContent;
			updateAll();
		};
		
		userSetInputCancel.type = "BUTTON";
		userSetInputCancel.value = "Cancel";
		userSetInputCancel.userMenuC = this;
		userSetInputCancel.savedContent = this.savedContent;
		userSetInputCancel.commentObj = this.commentObj;
		userSetInputCancel.onclick = function() {
			
			this.userMenuC.isSetupActive = 0;
			getCommentContentElement(this.commentObj).innerHTML = this.savedContent;
		};
		
		
		if (userName == this.commentUserName) {
			userSetInputColor.value = ownCommentColor;
			userSetInputSet.isOwn = 1;
			userSetInputReset.isOwn = 1;
		} else if (isTrollComment(this.commentObj)) {
			userSetInputSet.isTroll = "1";
			userSetInputReset.isTroll = "1";
			userSetInputColor.value = trollCommentColor;
		} else {
			userSetInputColor.value = getUserColorAssociated(this.commentObj);
		}
		
		userSetP.appendChild(userSetLabel1);
		userSetP.appendChild(userSetInputColor);
		userSetP.appendChild(userSetInputSet);
		userSetP.appendChild(userSetInputReset);
		userSetP.appendChild(userSetInputCancel);
		cont.appendChild(userSetP);		
	};
	
	subm.insertBefore(userMenuC, subm.firstChild);
	subm.insertBefore(userMenuT, userMenuC);
			
	umCnt++;
}


// --- insert jump to first new comment ----
if ((document.location.pathname.indexOf("/node") == 0)||
	(document.location.pathname.indexOf("/cikkek") == 0)||
	(document.location.pathname.indexOf("/promo") == 0)) {
		
	var theNode = document.getElementsByClassName('node ')[0];
	var theNodeChilds = theNode.childNodes;
	var theNodeLinks = null;
	for (i=0;i<theNodeChilds.length;i++) {
		if ((theNodeChilds.item(i) instanceof HTMLDivElement)&&(theNodeChilds.item(i).getAttribute('class') == 'links') ) {
			theNodeLinks = theNodeChilds.item(i);
		}
	}

	if (theNodeLinks != null) {
		var theNodeLinksLink = document.createElement('A');

		theNodeLinksLink.innerHTML = "Ugrás az első új hozzászólásra";
		theNodeLinksLink.href = "#new";
		theNodeLinksLink.style.cssFloat = "right";

		theNodeLinks.appendChild(theNodeLinksLink);
	}
}
	
// --- MENU ---
var div2 = document.createElement("DIV");
if (hiddenCommentsCount > 0) {	
	rightMenuText = hiddenCommentsCount+' beszólás elrejtve';
}
div2.style.cssFloat = 'right';
div2.style.color = 'darkgray';
div2.style.width = '200px';
div2.style.textAlign = 'right';

// right menu items

// title color : #D8D8C4
// body color: #F6F6EB
// text color: #363636		

// the right menu html

div2.innerHTML = '<div id="toppopupmenu" style="position: absolute; border: 1px solid #B7B787; color: #363636; background-color: #F6F6EB; width: 196px; text-align: left; padding: 0px; display: none" '+
'onmouseout="javascript: this.style.display = \'none\'"><div style="background-color: #D8D8C4; color: #000000; font-weight: bold; text-align: center; padding: 2px; border-bottom: 1px solid #B7B787;">MENÜ</div><div style="padding: 2px;" onmouseover="javascript: document.getElementById(\'toppopupmenu\').style.display = \'block\'">'+
'<a href="http://hup.hu/user/'+userID+'/track">Tracker</a><br/>'+
//'<a href="#" onclick="alert(documentGetKeyValue(\'userName\'))">Test</a><br/>'+
'<a href="#" onclick="switchTrollShow()">Trollok elrejtése / megjelenítése</a><br/>'+
'<a href="#" onclick="var comments = document.getElementsByClassName(\'comment\'); for (i=0;i<comments.length;i++) {comments[i].style.display = \'block\';} document.getElementById(\'toppopupmenu\').style.display = \'none\'; document.getElementById(\'rightmenutext\').innerHTML = \'MENÜ\';">Elrejtettek megjelenítése</a><br/>'+
'<hr><div style="font-weight: bold" title="Click a névre, és többé nem troll a troll :)">UnTroll lista:</div><div id="menuTrollList">&nbsp;</div><br/>'+
'</div></div><div id="rightmenutext" onmouseover="javascript: document.getElementById(\'toppopupmenu\').style.display=\'block\'">'+rightMenuText+'</div>';

topnav.appendChild(div2);




updateAll();



// BACKGROUND PROCESSES

function checkNewReplies() {
	var newReplie = 0;
			
	var xmlReq = new XMLHttpRequest();
	xmlReq.onreadystatechange = function() {
		if (xmlReq.readyState != 4) return;
		
		var responseDIV = document.createElement('DIV');
		responseDIV.innerHTML = xmlReq.responseText;
		
		var newRepliesTD = responseDIV.getElementsByClassName('replies');
		for (i=0;i<newRepliesTD.length;i++) {
			var newReplies = newRepliesTD[i].getElementsByTagName('A');
			if (newReplies.length > 0) {
				newReplie += parseInt(newReplies[0].innerHTML);
			}			
		}
		
		responseDIV = null;
					
		if (newReplie > 0) {
			rightMenuText = '<span style="color: red">'+newReplie+' új beszólás</span>&nbsp;MENÜ';
			document.getElementById('rightmenutext').innerHTML = rightMenuText;
			document.title = originalTitle+' ['+newReplie+' új beszólás]';
		} else {
			document.getElementById('rightmenutext').innerHTML = 'MENÜ';
			document.title = originalTitle;
		}		
	}

	xmlReq.open('GET', 'http://hup.hu/user/'+userID+'/track', true);
	xmlReq.send();
}
checkNewReplies();
setInterval(checkNewReplies, 30000);

// END



/**
 * jscolor, JavaScript Color Picker
 *
 * @version 1.3.9
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author  Jan Odvarko, http://odvarko.cz
 * @created 2008-06-15
 * @updated 2011-07-28
 * @link    http://jscolor.com
 */


var jscolor = {


	dir : 'chrome://hupbeszolas/content/', // location of jscolor directory (leave empty to autodetect)
	bindClass : 'color', // class name
	binding : true, // automatic binding via <input class="...">
	preloading : true, // use image preloading?


	install : function() {
		jscolor.addEvent(window, 'load', jscolor.init);
	},


	init : function() {
		if(jscolor.binding) {
			jscolor.bind();
		}
		if(jscolor.preloading) {
			jscolor.preload();
		}
	},


	getDir : function() {
		if(!jscolor.dir) {
			var detected = jscolor.detectDir();
			jscolor.dir = detected!==false ? detected : 'jscolor/';
		}
		return jscolor.dir;
	},


	detectDir : function() {
		var base = location.href;

		var e = document.getElementsByTagName('base');
		for(var i=0; i<e.length; i+=1) {
			if(e[i].href) { base = e[i].href; }
		}

		var e = document.getElementsByTagName('script');
		for(var i=0; i<e.length; i+=1) {
			if(e[i].src && /(^|\/)jscolor\.js([?#].*)?$/i.test(e[i].src)) {
				var src = new jscolor.URI(e[i].src);
				var srcAbs = src.toAbsolute(base);
				srcAbs.path = srcAbs.path.replace(/[^\/]+$/, ''); // remove filename
				srcAbs.query = null;
				srcAbs.fragment = null;
				return srcAbs.toString();
			}
		}
		return false;
	},


	bind : function() {
		var matchClass = new RegExp('(^|\\s)('+jscolor.bindClass+')\\s*(\\{[^}]*\\})?', 'i');
		var e = document.getElementsByTagName('input');
		for(var i=0; i<e.length; i+=1) {
			var m;
			if(!e[i].color && e[i].className && (m = e[i].className.match(matchClass))) {
				var prop = {};
				if(m[3]) {
					try {
						eval('prop='+m[3]);
					} catch(eInvalidProp) {}
				}
				e[i].color = new jscolor.color(e[i], prop);
			}
		}
	},


	preload : function() {
		for(var fn in jscolor.imgRequire) {
			if(jscolor.imgRequire.hasOwnProperty(fn)) {
				jscolor.loadImage(fn);
			}
		}
	},


	images : {
		pad : [ 181, 101 ],
		sld : [ 16, 101 ],
		cross : [ 15, 15 ],
		arrow : [ 7, 11 ]
	},


	imgRequire : {},
	imgLoaded : {},


	requireImage : function(filename) {
		jscolor.imgRequire[filename] = true;
	},


	loadImage : function(filename) {
		if(!jscolor.imgLoaded[filename]) {
			jscolor.imgLoaded[filename] = new Image();
			jscolor.imgLoaded[filename].src = jscolor.getDir()+filename;
		}
	},


	fetchElement : function(mixed) {
		return typeof mixed === 'string' ? document.getElementById(mixed) : mixed;
	},


	addEvent : function(el, evnt, func) {
		if(el.addEventListener) {
			el.addEventListener(evnt, func, false);
		} else if(el.attachEvent) {
			el.attachEvent('on'+evnt, func);
		}
	},


	fireEvent : function(el, evnt) {
		if(!el) {
			return;
		}
		if(document.createEvent) {
			var ev = document.createEvent('HTMLEvents');
			ev.initEvent(evnt, true, true);
			el.dispatchEvent(ev);
		} else if(document.createEventObject) {
			var ev = document.createEventObject();
			el.fireEvent('on'+evnt, ev);
		} else if(el['on'+evnt]) { // alternatively use the traditional event model (IE5)
			el['on'+evnt]();
		}
	},


	getElementPos : function(e) {
		var e1=e, e2=e;
		var x=0, y=0;
		if(e1.offsetParent) {
			do {
				x += e1.offsetLeft;
				y += e1.offsetTop;
			} while(e1 = e1.offsetParent);
		}
		while((e2 = e2.parentNode) && e2.nodeName.toUpperCase() !== 'BODY') {
			x -= e2.scrollLeft;
			y -= e2.scrollTop;
		}
		return [x, y];
	},


	getElementSize : function(e) {
		return [e.offsetWidth, e.offsetHeight];
	},


	getRelMousePos : function(e) {
		var x = 0, y = 0;
		if (!e) { e = window.event; }
		if (typeof e.offsetX === 'number') {
			x = e.offsetX;
			y = e.offsetY;
		} else if (typeof e.layerX === 'number') {
			x = e.layerX;
			y = e.layerY;
		}
		return { x: x, y: y };
	},


	getViewPos : function() {
		if(typeof window.pageYOffset === 'number') {
			return [window.pageXOffset, window.pageYOffset];
		} else if(document.body && (document.body.scrollLeft || document.body.scrollTop)) {
			return [document.body.scrollLeft, document.body.scrollTop];
		} else if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
			return [document.documentElement.scrollLeft, document.documentElement.scrollTop];
		} else {
			return [0, 0];
		}
	},


	getViewSize : function() {
		if(typeof window.innerWidth === 'number') {
			return [window.innerWidth, window.innerHeight];
		} else if(document.body && (document.body.clientWidth || document.body.clientHeight)) {
			return [document.body.clientWidth, document.body.clientHeight];
		} else if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
			return [document.documentElement.clientWidth, document.documentElement.clientHeight];
		} else {
			return [0, 0];
		}
	},


	URI : function(uri) { // See RFC3986

		this.scheme = null;
		this.authority = null;
		this.path = '';
		this.query = null;
		this.fragment = null;

		this.parse = function(uri) {
			var m = uri.match(/^(([A-Za-z][0-9A-Za-z+.-]*)(:))?((\/\/)([^\/?#]*))?([^?#]*)((\?)([^#]*))?((#)(.*))?/);
			this.scheme = m[3] ? m[2] : null;
			this.authority = m[5] ? m[6] : null;
			this.path = m[7];
			this.query = m[9] ? m[10] : null;
			this.fragment = m[12] ? m[13] : null;
			return this;
		};

		this.toString = function() {
			var result = '';
			if(this.scheme !== null) { result = result + this.scheme + ':'; }
			if(this.authority !== null) { result = result + '//' + this.authority; }
			if(this.path !== null) { result = result + this.path; }
			if(this.query !== null) { result = result + '?' + this.query; }
			if(this.fragment !== null) { result = result + '#' + this.fragment; }
			return result;
		};

		this.toAbsolute = function(base) {
			var base = new jscolor.URI(base);
			var r = this;
			var t = new jscolor.URI;

			if(base.scheme === null) { return false; }

			if(r.scheme !== null && r.scheme.toLowerCase() === base.scheme.toLowerCase()) {
				r.scheme = null;
			}

			if(r.scheme !== null) {
				t.scheme = r.scheme;
				t.authority = r.authority;
				t.path = removeDotSegments(r.path);
				t.query = r.query;
			} else {
				if(r.authority !== null) {
					t.authority = r.authority;
					t.path = removeDotSegments(r.path);
					t.query = r.query;
				} else {
					if(r.path === '') { // TODO: == or === ?
						t.path = base.path;
						if(r.query !== null) {
							t.query = r.query;
						} else {
							t.query = base.query;
						}
					} else {
						if(r.path.substr(0,1) === '/') {
							t.path = removeDotSegments(r.path);
						} else {
							if(base.authority !== null && base.path === '') { // TODO: == or === ?
								t.path = '/'+r.path;
							} else {
								t.path = base.path.replace(/[^\/]+$/,'')+r.path;
							}
							t.path = removeDotSegments(t.path);
						}
						t.query = r.query;
					}
					t.authority = base.authority;
				}
				t.scheme = base.scheme;
			}
			t.fragment = r.fragment;

			return t;
		};

		function removeDotSegments(path) {
			var out = '';
			while(path) {
				if(path.substr(0,3)==='../' || path.substr(0,2)==='./') {
					path = path.replace(/^\.+/,'').substr(1);
				} else if(path.substr(0,3)==='/./' || path==='/.') {
					path = '/'+path.substr(3);
				} else if(path.substr(0,4)==='/../' || path==='/..') {
					path = '/'+path.substr(4);
					out = out.replace(/\/?[^\/]*$/, '');
				} else if(path==='.' || path==='..') {
					path = '';
				} else {
					var rm = path.match(/^\/?[^\/]*/)[0];
					path = path.substr(rm.length);
					out = out + rm;
				}
			}
			return out;
		}

		if(uri) {
			this.parse(uri);
		}

	},


	/*
	 * Usage example:
	 * var myColor = new jscolor.color(myInputElement)
	 */

	color : function(target, prop) {


		this.required = true; // refuse empty values?
		this.adjust = true; // adjust value to uniform notation?
		this.hash = false; // prefix color with # symbol?
		this.caps = true; // uppercase?
		this.slider = true; // show the value/saturation slider?
		this.valueElement = target; // value holder
		this.styleElement = target; // where to reflect current color
		this.hsv = [0, 0, 1]; // read-only  0-6, 0-1, 0-1
		this.rgb = [1, 1, 1]; // read-only  0-1, 0-1, 0-1

		this.pickerOnfocus = true; // display picker on focus?
		this.pickerMode = 'HSV'; // HSV | HVS
		this.pickerPosition = 'bottom'; // left | right | top | bottom
		this.pickerButtonHeight = 20; // px
		this.pickerClosable = false;
		this.pickerCloseText = 'Close';
		this.pickerButtonColor = 'ButtonText'; // px
		this.pickerFace = 10; // px
		this.pickerFaceColor = 'ThreeDFace'; // CSS color
		this.pickerBorder = 1; // px
		this.pickerBorderColor = 'ThreeDHighlight ThreeDShadow ThreeDShadow ThreeDHighlight'; // CSS color
		this.pickerInset = 1; // px
		this.pickerInsetColor = 'ThreeDShadow ThreeDHighlight ThreeDHighlight ThreeDShadow'; // CSS color
		this.pickerZIndex = 10000;


		for(var p in prop) {
			if(prop.hasOwnProperty(p)) {
				this[p] = prop[p];
			}
		}


		this.hidePicker = function() {
			if(isPickerOwner()) {
				removePicker();
			}
		};


		this.showPicker = function() {
			if(!isPickerOwner()) {
				var tp = jscolor.getElementPos(target); // target pos
				var ts = jscolor.getElementSize(target); // target size
				var vp = jscolor.getViewPos(); // view pos
				var vs = jscolor.getViewSize(); // view size
				var ps = getPickerDims(this); // picker size
				var a, b, c;
				switch(this.pickerPosition.toLowerCase()) {
					case 'left': a=1; b=0; c=-1; break;
					case 'right':a=1; b=0; c=1; break;
					case 'top':  a=0; b=1; c=-1; break;
					default:     a=0; b=1; c=1; break;
				}
				var l = (ts[b]+ps[b])/2;
				var pp = [ // picker pos
					-vp[a]+tp[a]+ps[a] > vs[a] ?
						(-vp[a]+tp[a]+ts[a]/2 > vs[a]/2 && tp[a]+ts[a]-ps[a] >= 0 ? tp[a]+ts[a]-ps[a] : tp[a]) :
						tp[a],
					-vp[b]+tp[b]+ts[b]+ps[b]-l+l*c > vs[b] ?
						(-vp[b]+tp[b]+ts[b]/2 > vs[b]/2 && tp[b]+ts[b]-l-l*c >= 0 ? tp[b]+ts[b]-l-l*c : tp[b]+ts[b]-l+l*c) :
						(tp[b]+ts[b]-l+l*c >= 0 ? tp[b]+ts[b]-l+l*c : tp[b]+ts[b]-l-l*c)
				];
				drawPicker(pp[a], pp[b]);
			}
		};


		this.importColor = function() {
			if(!valueElement) {
				this.exportColor();
			} else {
				if(!this.adjust) {
					if(!this.fromString(valueElement.value, leaveValue)) {
						styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
						styleElement.style.color = styleElement.jscStyle.color;
						this.exportColor(leaveValue | leaveStyle);
					}
				} else if(!this.required && /^\s*$/.test(valueElement.value)) {
					valueElement.value = '';
					styleElement.style.backgroundColor = styleElement.jscStyle.backgroundColor;
					styleElement.style.color = styleElement.jscStyle.color;
					this.exportColor(leaveValue | leaveStyle);

				} else if(this.fromString(valueElement.value)) {
					// OK
				} else {
					this.exportColor();
				}
			}
		};


		this.exportColor = function(flags) {
			if(!(flags & leaveValue) && valueElement) {
				var value = this.toString();
				if(this.caps) { value = value.toUpperCase(); }
				if(this.hash) { value = '#'+value; }
				valueElement.value = value;
			}
			if(!(flags & leaveStyle) && styleElement) {
				styleElement.style.backgroundColor =
					'#'+this.toString();
				styleElement.style.color =
					0.213 * this.rgb[0] +
					0.715 * this.rgb[1] +
					0.072 * this.rgb[2]
					< 0.5 ? '#FFF' : '#000';
			}
			if(!(flags & leavePad) && isPickerOwner()) {
				redrawPad();
			}
			if(!(flags & leaveSld) && isPickerOwner()) {
				redrawSld();
			}
		};


		this.fromHSV = function(h, s, v, flags) { // null = don't change
			h<0 && (h=0) || h>6 && (h=6);
			s<0 && (s=0) || s>1 && (s=1);
			v<0 && (v=0) || v>1 && (v=1);
			this.rgb = HSV_RGB(
				h===null ? this.hsv[0] : (this.hsv[0]=h),
				s===null ? this.hsv[1] : (this.hsv[1]=s),
				v===null ? this.hsv[2] : (this.hsv[2]=v)
			);
			this.exportColor(flags);
		};


		this.fromRGB = function(r, g, b, flags) { // null = don't change
			r<0 && (r=0) || r>1 && (r=1);
			g<0 && (g=0) || g>1 && (g=1);
			b<0 && (b=0) || b>1 && (b=1);
			var hsv = RGB_HSV(
				r===null ? this.rgb[0] : (this.rgb[0]=r),
				g===null ? this.rgb[1] : (this.rgb[1]=g),
				b===null ? this.rgb[2] : (this.rgb[2]=b)
			);
			if(hsv[0] !== null) {
				this.hsv[0] = hsv[0];
			}
			if(hsv[2] !== 0) {
				this.hsv[1] = hsv[1];
			}
			this.hsv[2] = hsv[2];
			this.exportColor(flags);
		};


		this.fromString = function(hex, flags) {
			var m = hex.match(/^\W*([0-9A-F]{3}([0-9A-F]{3})?)\W*$/i);
			if(!m) {
				return false;
			} else {
				if(m[1].length === 6) { // 6-char notation
					this.fromRGB(
						parseInt(m[1].substr(0,2),16) / 255,
						parseInt(m[1].substr(2,2),16) / 255,
						parseInt(m[1].substr(4,2),16) / 255,
						flags
					);
				} else { // 3-char notation
					this.fromRGB(
						parseInt(m[1].charAt(0)+m[1].charAt(0),16) / 255,
						parseInt(m[1].charAt(1)+m[1].charAt(1),16) / 255,
						parseInt(m[1].charAt(2)+m[1].charAt(2),16) / 255,
						flags
					);
				}
				return true;
			}
		};


		this.toString = function() {
			return (
				(0x100 | Math.round(255*this.rgb[0])).toString(16).substr(1) +
				(0x100 | Math.round(255*this.rgb[1])).toString(16).substr(1) +
				(0x100 | Math.round(255*this.rgb[2])).toString(16).substr(1)
			);
		};


		function RGB_HSV(r, g, b) {
			var n = Math.min(Math.min(r,g),b);
			var v = Math.max(Math.max(r,g),b);
			var m = v - n;
			if(m === 0) { return [ null, 0, v ]; }
			var h = r===n ? 3+(b-g)/m : (g===n ? 5+(r-b)/m : 1+(g-r)/m);
			return [ h===6?0:h, m/v, v ];
		}


		function HSV_RGB(h, s, v) {
			if(h === null) { return [ v, v, v ]; }
			var i = Math.floor(h);
			var f = i%2 ? h-i : 1-(h-i);
			var m = v * (1 - s);
			var n = v * (1 - s*f);
			switch(i) {
				case 6:
				case 0: return [v,n,m];
				case 1: return [n,v,m];
				case 2: return [m,v,n];
				case 3: return [m,n,v];
				case 4: return [n,m,v];
				case 5: return [v,m,n];
			}
		}


		function removePicker() {
			delete jscolor.picker.owner;
			document.getElementsByTagName('body')[0].removeChild(jscolor.picker.boxB);
		}


		function drawPicker(x, y) {
			if(!jscolor.picker) {
				jscolor.picker = {
					box : document.createElement('div'),
					boxB : document.createElement('div'),
					pad : document.createElement('div'),
					padB : document.createElement('div'),
					padM : document.createElement('div'),
					sld : document.createElement('div'),
					sldB : document.createElement('div'),
					sldM : document.createElement('div'),
					btn : document.createElement('div'),
					btnS : document.createElement('span'),
					btnT : document.createTextNode(THIS.pickerCloseText)
				};
				for(var i=0,segSize=4; i<jscolor.images.sld[1]; i+=segSize) {
					var seg = document.createElement('div');
					seg.style.height = segSize+'px';
					seg.style.fontSize = '1px';
					seg.style.lineHeight = '0';
					jscolor.picker.sld.appendChild(seg);
				}
				jscolor.picker.sldB.appendChild(jscolor.picker.sld);
				jscolor.picker.box.appendChild(jscolor.picker.sldB);
				jscolor.picker.box.appendChild(jscolor.picker.sldM);
				jscolor.picker.padB.appendChild(jscolor.picker.pad);
				jscolor.picker.box.appendChild(jscolor.picker.padB);
				jscolor.picker.box.appendChild(jscolor.picker.padM);
				jscolor.picker.btnS.appendChild(jscolor.picker.btnT);
				jscolor.picker.btn.appendChild(jscolor.picker.btnS);
				jscolor.picker.box.appendChild(jscolor.picker.btn);
				jscolor.picker.boxB.appendChild(jscolor.picker.box);
			}

			var p = jscolor.picker;

			// controls interaction
			p.box.onmouseup =
			p.box.onmouseout = function() { target.focus(); };
			p.box.onmousedown = function() { abortBlur=true; };
			p.box.onmousemove = function(e) {
				if (holdPad || holdSld) {
					holdPad && setPad(e);
					holdSld && setSld(e);
					if (document.selection) {
						document.selection.empty();
					} else if (window.getSelection) {
						window.getSelection().removeAllRanges();
					}
				}
			};
			p.padM.onmouseup =
			p.padM.onmouseout = function() { if(holdPad) { holdPad=false; jscolor.fireEvent(valueElement,'change'); } };
			p.padM.onmousedown = function(e) { holdPad=true; setPad(e); };
			p.sldM.onmouseup =
			p.sldM.onmouseout = function() { if(holdSld) { holdSld=false; jscolor.fireEvent(valueElement,'change'); } };
			p.sldM.onmousedown = function(e) { holdSld=true; setSld(e); };

			// picker
			var dims = getPickerDims(THIS);
			p.box.style.width = dims[0] + 'px';
			p.box.style.height = dims[1] + 'px';

			// picker border
			p.boxB.style.position = 'absolute';
			p.boxB.style.clear = 'both';
			p.boxB.style.left = x+'px';
			p.boxB.style.top = y+'px';
			p.boxB.style.zIndex = THIS.pickerZIndex;
			p.boxB.style.border = THIS.pickerBorder+'px solid';
			p.boxB.style.borderColor = THIS.pickerBorderColor;
			p.boxB.style.background = THIS.pickerFaceColor;

			// pad image
			p.pad.style.width = jscolor.images.pad[0]+'px';
			p.pad.style.height = jscolor.images.pad[1]+'px';

			// pad border
			p.padB.style.position = 'absolute';
			p.padB.style.left = THIS.pickerFace+'px';
			p.padB.style.top = THIS.pickerFace+'px';
			p.padB.style.border = THIS.pickerInset+'px solid';
			p.padB.style.borderColor = THIS.pickerInsetColor;

			// pad mouse area
			p.padM.style.position = 'absolute';
			p.padM.style.left = '0';
			p.padM.style.top = '0';
			p.padM.style.width = THIS.pickerFace + 2*THIS.pickerInset + jscolor.images.pad[0] + jscolor.images.arrow[0] + 'px';
			p.padM.style.height = p.box.style.height;
			p.padM.style.cursor = 'crosshair';

			// slider image
			p.sld.style.overflow = 'hidden';
			p.sld.style.width = jscolor.images.sld[0]+'px';
			p.sld.style.height = jscolor.images.sld[1]+'px';

			// slider border
			p.sldB.style.display = THIS.slider ? 'block' : 'none';
			p.sldB.style.position = 'absolute';
			p.sldB.style.right = THIS.pickerFace+'px';
			p.sldB.style.top = THIS.pickerFace+'px';
			p.sldB.style.border = THIS.pickerInset+'px solid';
			p.sldB.style.borderColor = THIS.pickerInsetColor;

			// slider mouse area
			p.sldM.style.display = THIS.slider ? 'block' : 'none';
			p.sldM.style.position = 'absolute';
			p.sldM.style.right = '0';
			p.sldM.style.top = '0';
			p.sldM.style.width = jscolor.images.sld[0] + jscolor.images.arrow[0] + THIS.pickerFace + 2*THIS.pickerInset + 'px';
			p.sldM.style.height = p.box.style.height;
			try {
				p.sldM.style.cursor = 'pointer';
			} catch(eOldIE) {
				p.sldM.style.cursor = 'hand';
			}

			// "close" button
			function setBtnBorder() {
				var insetColors = THIS.pickerInsetColor.split(/\s+/);
				var pickerOutsetColor = insetColors.length < 2 ? insetColors[0] : insetColors[1] + ' ' + insetColors[0] + ' ' + insetColors[0] + ' ' + insetColors[1];
				p.btn.style.borderColor = pickerOutsetColor;
			}
			p.btn.style.display = THIS.pickerClosable ? 'block' : 'none';
			p.btn.style.position = 'absolute';
			p.btn.style.left = THIS.pickerFace + 'px';
			p.btn.style.bottom = THIS.pickerFace + 'px';
			p.btn.style.padding = '0 15px';
			p.btn.style.height = '18px';
			p.btn.style.border = THIS.pickerInset + 'px solid';
			setBtnBorder();
			p.btn.style.color = THIS.pickerButtonColor;
			p.btn.style.font = '12px sans-serif';
			p.btn.style.textAlign = 'center';
			try {
				p.btn.style.cursor = 'pointer';
			} catch(eOldIE) {
				p.btn.style.cursor = 'hand';
			}
			p.btn.onmousedown = function () {
				THIS.hidePicker();
			};
			p.btnS.style.lineHeight = p.btn.style.height;

			// load images in optimal order
			switch(modeID) {
				case 0: var padImg = 'hs.png'; break;
				case 1: var padImg = 'hv.png'; break;
			}
			p.padM.style.backgroundImage = "url('"+jscolor.getDir()+"cross.gif')";
			p.padM.style.backgroundRepeat = "no-repeat";
			p.sldM.style.backgroundImage = "url('"+jscolor.getDir()+"arrow.gif')";
			p.sldM.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundImage = "url('"+jscolor.getDir()+padImg+"')";
			p.pad.style.backgroundRepeat = "no-repeat";
			p.pad.style.backgroundPosition = "0 0";

			// place pointers
			redrawPad();
			redrawSld();

			jscolor.picker.owner = THIS;
			document.getElementsByTagName('body')[0].appendChild(p.boxB);
		}


		function getPickerDims(o) {
			var dims = [
				2*o.pickerInset + 2*o.pickerFace + jscolor.images.pad[0] +
					(o.slider ? 2*o.pickerInset + 2*jscolor.images.arrow[0] + jscolor.images.sld[0] : 0),
				o.pickerClosable ?
					4*o.pickerInset + 3*o.pickerFace + jscolor.images.pad[1] + o.pickerButtonHeight :
					2*o.pickerInset + 2*o.pickerFace + jscolor.images.pad[1]
			];
			return dims;
		}


		function redrawPad() {
			// redraw the pad pointer
			switch(modeID) {
				case 0: var yComponent = 1; break;
				case 1: var yComponent = 2; break;
			}
			var x = Math.round((THIS.hsv[0]/6) * (jscolor.images.pad[0]-1));
			var y = Math.round((1-THIS.hsv[yComponent]) * (jscolor.images.pad[1]-1));
			jscolor.picker.padM.style.backgroundPosition =
				(THIS.pickerFace+THIS.pickerInset+x - Math.floor(jscolor.images.cross[0]/2)) + 'px ' +
				(THIS.pickerFace+THIS.pickerInset+y - Math.floor(jscolor.images.cross[1]/2)) + 'px';

			// redraw the slider image
			var seg = jscolor.picker.sld.childNodes;

			switch(modeID) {
				case 0:
					var rgb = HSV_RGB(THIS.hsv[0], THIS.hsv[1], 1);
					for(var i=0; i<seg.length; i+=1) {
						seg[i].style.backgroundColor = 'rgb('+
							(rgb[0]*(1-i/seg.length)*100)+'%,'+
							(rgb[1]*(1-i/seg.length)*100)+'%,'+
							(rgb[2]*(1-i/seg.length)*100)+'%)';
					}
					break;
				case 1:
					var rgb, s, c = [ THIS.hsv[2], 0, 0 ];
					var i = Math.floor(THIS.hsv[0]);
					var f = i%2 ? THIS.hsv[0]-i : 1-(THIS.hsv[0]-i);
					switch(i) {
						case 6:
						case 0: rgb=[0,1,2]; break;
						case 1: rgb=[1,0,2]; break;
						case 2: rgb=[2,0,1]; break;
						case 3: rgb=[2,1,0]; break;
						case 4: rgb=[1,2,0]; break;
						case 5: rgb=[0,2,1]; break;
					}
					for(var i=0; i<seg.length; i+=1) {
						s = 1 - 1/(seg.length-1)*i;
						c[1] = c[0] * (1 - s*f);
						c[2] = c[0] * (1 - s);
						seg[i].style.backgroundColor = 'rgb('+
							(c[rgb[0]]*100)+'%,'+
							(c[rgb[1]]*100)+'%,'+
							(c[rgb[2]]*100)+'%)';
					}
					break;
			}
		}


		function redrawSld() {
			// redraw the slider pointer
			switch(modeID) {
				case 0: var yComponent = 2; break;
				case 1: var yComponent = 1; break;
			}
			var y = Math.round((1-THIS.hsv[yComponent]) * (jscolor.images.sld[1]-1));
			jscolor.picker.sldM.style.backgroundPosition =
				'0 ' + (THIS.pickerFace+THIS.pickerInset+y - Math.floor(jscolor.images.arrow[1]/2)) + 'px';
		}


		function isPickerOwner() {
			return jscolor.picker && jscolor.picker.owner === THIS;
		}


		function blurTarget() {
			if(valueElement === target) {
				THIS.importColor();
			}
			if(THIS.pickerOnfocus) {
				THIS.hidePicker();
			}
		}


		function blurValue() {
			if(valueElement !== target) {
				THIS.importColor();
			}
		}


		function setPad(e) {
			var mpos = jscolor.getRelMousePos(e);
			var x = mpos.x - THIS.pickerFace - THIS.pickerInset;
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch(modeID) {
				case 0: THIS.fromHSV(x*(6/(jscolor.images.pad[0]-1)), 1 - y/(jscolor.images.pad[1]-1), null, leaveSld); break;
				case 1: THIS.fromHSV(x*(6/(jscolor.images.pad[0]-1)), null, 1 - y/(jscolor.images.pad[1]-1), leaveSld); break;
			}
		}


		function setSld(e) {
			var mpos = jscolor.getRelMousePos(e);
			var y = mpos.y - THIS.pickerFace - THIS.pickerInset;
			switch(modeID) {
				case 0: THIS.fromHSV(null, null, 1 - y/(jscolor.images.sld[1]-1), leavePad); break;
				case 1: THIS.fromHSV(null, 1 - y/(jscolor.images.sld[1]-1), null, leavePad); break;
			}
		}


		var THIS = this;
		var modeID = this.pickerMode.toLowerCase()==='hvs' ? 1 : 0;
		var abortBlur = false;
		var
			valueElement = jscolor.fetchElement(this.valueElement),
			styleElement = jscolor.fetchElement(this.styleElement);
		var
			holdPad = false,
			holdSld = false;
		var
			leaveValue = 1<<0,
			leaveStyle = 1<<1,
			leavePad = 1<<2,
			leaveSld = 1<<3;

		// target
		jscolor.addEvent(target, 'focus', function() {
			if(THIS.pickerOnfocus) { THIS.showPicker(); }
		});
		jscolor.addEvent(target, 'blur', function() {
			if(!abortBlur) {
				window.setTimeout(function(){ abortBlur || blurTarget(); abortBlur=false; }, 0);
			} else {
				abortBlur = false;
			}
		});

		// valueElement
		if(valueElement) {
			var updateField = function() {
				THIS.fromString(valueElement.value, leaveValue);
			};
			jscolor.addEvent(valueElement, 'keyup', updateField);
			jscolor.addEvent(valueElement, 'input', updateField);
			jscolor.addEvent(valueElement, 'blur', blurValue);
			valueElement.setAttribute('autocomplete', 'off');
		}

		// styleElement
		if(styleElement) {
			styleElement.jscStyle = {
				backgroundColor : styleElement.style.backgroundColor,
				color : styleElement.style.color
			};
		}

		// require images
		switch(modeID) {
			case 0: jscolor.requireImage('hs.png'); break;
			case 1: jscolor.requireImage('hv.png'); break;
		}
		jscolor.requireImage('cross.gif');
		jscolor.requireImage('arrow.gif');

		this.importColor();
	}

};


jscolor.install();

console.log("HUPbeszolas has been loaded");
