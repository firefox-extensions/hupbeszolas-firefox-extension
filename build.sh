#!/usr/bin/env bash

VERSION=$1
RELEASES_DIR=releases

if [ ! -d "${RELEASES_DIR}" ]; then
    mkdir "${RELEASES_DIR}"
    if [ $? -ne 0 ]; then
        echo "Could not create release directory: ${RELEASES_DIR}" >&2
        exit 1
    fi
fi

if [ ! -z "$VERSION" ]; then
    if [[ "${OSTYPE}" = darwin* ]]; then
        sed -i '' "/\"version\":/s/\"version\": \".*\"/\"version\": \"${VERSION}\"/" manifest.json
    else
        sed -i "/\"version\":/s/\"version\": \".*\"/\"version\": \"${VERSION}\"/" manifest.json
    fi
else
    VERSION=$(sed -n '/"version":/s/.*"version": "\(.*\)",.*/\1/p' manifest.json)
fi

echo -n "Building version ${VERSION}..."

zip -r "${RELEASES_DIR}/hupbeszolas-${VERSION}.xpi" manifest.json hupbeszolas >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo " done"
else
    echo " failed, error code: $?"
fi
