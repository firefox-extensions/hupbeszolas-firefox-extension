﻿var doc = document;

if (
    (doc.location.host == "hup.hu") && (
        (doc.location.pathname.indexOf("/") == 0) ||
        (doc.location.pathname.indexOf("/user") == 0) ||
        (doc.location.pathname.indexOf("/node") == 0) ||
        (doc.location.pathname.indexOf("/cikkek") == 0) ||
        (doc.location.pathname.indexOf("/promo") == 0)
    )) {

    // --- LIKE SUBMIT ---
    var likeSubmitLI = doc.location.pathname.lastIndexOf('&');
    if (likeSubmitLI > 0) {
        if (doc.location.pathname.substring(likeSubmitLI + 1) == 'plusone') {
            doc.getElementById('edit-comment').value = '+1';
            doc.getElementById('edit-submit').click();
        }
    }

    // --- load UTIL ---
    var utilScript = doc.createElement('SCRIPT');
    utilScript.type = 'text/javascript';
    utilScript.src = chrome.extension.getURL("hupbeszolas/hupbeszolasUtil.js");
    doc.getElementsByTagName('HEAD')[0].appendChild(utilScript);
}