# Hupbeszólás Firefox extension

***Not developed anymore...***

This is a newer, webextension version and compatible with Firefox version 45+. 

[For Safari extension click here](safari)

### Download

 * [Version 0.5.0](https://gitlab.com/firefox-extensions/hupbeszolas-firefox-extension/raw/master/releases/hupbeszolas-0.5.0-fx.xpi)
 * [Version 0.4.2](https://gitlab.com/firefox-extensions/hupbeszolas-firefox-extension/raw/master/releases/hupbeszolas-0.4.2-fx.xpi)

### Old

 * [Version 0.4.1](https://gitlab.com/firefox-extensions/hupbeszolas-firefox-extension/raw/master/releases/hup_beszolas-0.4.1-fx.xpi)  
   This version on the latest old extension version (XUL overlay). **Deprecated**.

### Changelog

 * 0.5.0  
   Added parent comment hover

 * 0.4.2  
   Addon has been converted to webextension
   
